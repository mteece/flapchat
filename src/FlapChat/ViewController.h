//
//  ViewController.h
//  FlapChat
//
//  Created by Matthew Teece on 5/29/15.
//  Copyright (c) 2015 Matthew Teece. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : JSQMessagesViewController

@property (copy, nonatomic) NSString *userName;
@property (copy, nonatomic) NSMutableArray *messages; // Mutability is sketchy.
@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubble;
@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubble;
@property (nonatomic) OTSession *session;
@property (nonatomic) OTConnection *connection;
@property (nonatomic) NSMutableArray *connections;

@end

