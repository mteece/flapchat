//
//  ViewController.m
//  FlapChat
//
//  Created by Matthew Teece on 5/29/15.
//  Copyright (c) 2015 Matthew Teece. All rights reserved.
//

#import <JSQMessagesViewController/JSQMessages.h>
#import <OpenTok/OpenTok.h>

#import "ViewController.h"

@interface ViewController () <OTSessionDelegate>

@end

@implementation ViewController
{
    // private ivars
}

static NSString* const kApiKey = @"45247572";
static NSString* const kSessionId = @"1_MX40NTI0NzU3Mn5-MTQzMzE3MTY3MDE2MX5lSGIrdUhpeW1tajFBNmwvSUVrUjdjbHZ-fg";
static NSString* const kToken = @"T1==cGFydG5lcl9pZD00NTI0NzU3MiZzaWc9ZGIyNjIxMmRjZmNhNTlhOTY4M2QwMTFhZGYxOTAwN2JlOTcyYzM2Zjpyb2xlPXB1Ymxpc2hlciZzZXNzaW9uX2lkPTFfTVg0ME5USTBOelUzTW41LU1UUXpNekUzTVRZM01ERTJNWDVsU0dJcmRVaHBlVzF0YWpGQk5td3ZTVVZyVWpkamJIWi1mZyZjcmVhdGVfdGltZT0xNDMzMTcxNjgyJm5vbmNlPTAuMjk3MjY2ODQyMzkxOTIwNDcmZXhwaXJlX3RpbWU9MTQzMzI1ODA1MiZjb25uZWN0aW9uX2RhdGE9";

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // Class
    if (!_connections) _connections = [[NSMutableArray alloc] init];
    if (!_messages) _messages = [[NSMutableArray alloc] init];
    
    // Opentok
    _session = [[OTSession alloc] initWithApiKey:kApiKey
                                       sessionId:kSessionId
                                        delegate:self];
    
    OTError *error = nil;
    
    [_session connectWithToken:kToken error:&error];
    if (error) [self showAlert:[error localizedDescription]];

    // JSQMessage
    JSQMessagesBubbleImageFactory *factory = [[JSQMessagesBubbleImageFactory alloc] init];
    
    _userName = @"User 1";
    _incomingBubble = [factory incomingMessagesBubbleImageWithColor:[UIColor redColor]];
    _outgoingBubble = [factory outgoingMessagesBubbleImageWithColor:[UIColor blueColor]];
    
    
    /*
    for (int i = 0; i < 10; i++) {
        NSString *sender =  (i%2) ? [NSString stringWithFormat:@"%s", "User 2"] : _userName;
        JSQMessage *message = [[JSQMessage alloc] initWithSenderId:sender senderDisplayName:sender date:[NSDate date] text:@"Text"];
        [_messages addObject:message];
    }
     */
    [self.collectionView reloadData];
    self.senderDisplayName = self.userName;
    self.senderId = @"";
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/* laziness
- (NSMutableArray *)messages
{
    if (!_messages) _messages = [[NSMutableArray alloc] init];
    return _messages;
}
*/

#pragma mark - JSQMessages CollectionView DataSource

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return _messages[indexPath.row];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *data = _messages[indexPath.row];
    if ([data.senderId isEqual:self.senderId]) {
        return _outgoingBubble;
    } else {
        return _incomingBubble;
    }
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}


#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_messages count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Override point for customizing cells
     */
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    /**
     *  Configure almost *anything* on the cell
     *
     *  Text colors, label text, label colors, etc.
     *
     *
     *  DO NOT set `cell.textView.font` !
     *  Instead, you need to set `self.collectionView.collectionViewLayout.messageBubbleFont` to the font you want in `viewDidLoad`
     *
     *
     *  DO NOT manipulate cell layout information!
     *  Instead, override the properties you want on `self.collectionView.collectionViewLayout` from `viewDidLoad`
     */
    
    JSQMessage *msg = [_messages objectAtIndex:indexPath.item];
    
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString:self.senderId]) {
            cell.textView.textColor = [UIColor whiteColor];
        }
        else {
            cell.textView.textColor = [UIColor whiteColor];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    
    return cell;
}

#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date
{
    
    OTError *error = nil;
    
    NSString *type = [[NSString alloc] initWithFormat:@""];
    
    [_session signalWithType:type string:text connection:nil error:&error];
    
    if (error) {
        [self showAlert:[error localizedDescription]];
    } else {
        // TBD:
    }
}

- (void)didPressAccessoryButton:(UIButton *)sender
{

}

#pragma mark -
#pragma mark OTSession delegate callbacks

- (void) session:(OTSession*)session receivedSignalType:(NSString*)type fromConnection:(OTConnection*)connection withString:(NSString*)string
{
    NSLog(@"receivedSignalType (%@, %@, %@)", type, connection.connectionId, string);
    NSMutableString *incomingSender = [[NSMutableString alloc] init];
    
    if ([connection.connectionId isEqual:_connection.connectionId]) {
        incomingSender = [NSMutableString stringWithString:self.senderId];
    } else {
        incomingSender = [NSMutableString stringWithString:connection.connectionId];
    }
    
    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:connection.connectionId
                                             senderDisplayName:incomingSender
                                                          date:connection.creationTime
                                                          text:string];
    [_messages addObject:message];
    
    [self finishReceivingMessageAnimated:YES];
    

}

- (void) session:(OTSession*)session didFailWithError:(OTError*)error
{
    NSLog(@"didFailWithError: (%@)", error);
}

- (void)sessionDidDisconnect:(OTSession*)session
{
    NSString* alertMessage = [NSString stringWithFormat:@"Session disconnected: (%@)", session.sessionId];
    NSLog(@"sessionDidDisconnect (%@)", alertMessage);
    self.senderId = @"";
    _connection = nil;
    
}

- (void)sessionDidConnect:(OTSession*)session
{
    NSLog(@"sessionDidConnect (%@)", session.sessionId);
    self.senderId = session.connection.connectionId;
    _connection = session.connection;
    
}

- (void)session:(OTSession *)session connectionCreated:(OTConnection *)connection
{
    NSLog(@"connectionCreated (%@)", connection.connectionId);
    [_connections addObject:connection];
    
    
}

- (void)session:(OTSession *)session connectionDestroyed:(OTConnection *)connection
{
    NSLog(@"connectionDestroyed (%@)", connection.connectionId);
    [_connections removeObject:connection];
}

- (void)session:(OTSession*)session streamCreated:(OTStream*)stream
{
    
}

- (void)session:(OTSession*)session streamDestroyed:(OTStream*)stream
{
    
}


#pragma mark -
#pragma mark Class methods

- (void)showAlert:(NSString *)string
{
    // show alertview on main UI
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OTError"
                                                        message:string
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil] ;
        [alert show];
    });
}

@end
